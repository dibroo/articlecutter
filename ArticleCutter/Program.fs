﻿open FSharp.Data
open HtmlNode
open HandyTools
open HandyTools.Func

let newNode name attrs children = HtmlNode.NewElement(name, attrs, children)

let (|HText|HComment|HNode|) (node : HtmlNode) =
    match node.Name() with
    | "" when node.DirectInnerText().Length <> 0 
            -> HText node
    | ""    -> HComment
    | _     -> HNode node

type HtmlTree =
    | HtmlTreeNode of string * (string * string) list * HtmlTree list
    | HtmlTreeText of string
    | HtmlTreeComment

let rec htmlNodeToTree = function
    | HNode t ->
        let name, attrs = t.Name(), t.Attributes() |> List.map (fun x -> x.Name(), x.Value())
        let children = 
            t.Descendants(alwaysTrue, false) |> Seq.map htmlNodeToTree |> Seq.toList
        HtmlTreeNode (name, attrs, children)

    | HText t -> HtmlTreeText (t.InnerText())
    | HComment -> HtmlTreeComment

let allText (t : HtmlTree) : string =
    let rec get (t : System.Text.StringBuilder) = function
        | HtmlTreeNode(_, _, chld)  -> chld |> List.iter (get t)
        | HtmlTreeText text         -> t.AppendLine text |> ignore
        | _ -> ()
    let tb = new System.Text.StringBuilder()
    get tb t |/> tb.ToString()

let innerText = function
    | HtmlTreeNode (_, _, chld) -> 
        chld 
            |> List.choose (function HtmlTreeText t -> Some t | _ -> None)
            |> List.fold (argS2 (+) "\n" >> (+)) ""
    | HtmlTreeText t -> t | _ -> ""

let rec filterNodes filter recurse = function
    | HtmlTreeNode (n, a, chld) ->
        let chld =
            chld |> List.where filter |> 
                fun x -> if recurse then chld |> List.map (filterNodes filter recurse) else chld
        HtmlTreeNode (n, a, chld)
    | x -> x

let rec countNodes = function
    | HtmlTreeNode (_, _, chld) ->
        1 + (chld |> List.map countNodes |> List.sum)
    | HtmlTreeText _ -> 1 | _ -> 0
            

[<EntryPoint>]
let main argv =
    let html = HtmlDocument.Parse(@"<body><!--Comment-->Text<p>Text<b>Another Text</b></p></body>").Body()

    printfn "%s %d" ((htmlNodeToTree >> innerText) html) ((htmlNodeToTree >> countNodes) html)

    0
